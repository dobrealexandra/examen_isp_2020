package examen_isp_2020;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class S2 extends JFrame {


    S2() {
        setTitle("Calculator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 500);
        setVisible(true);
        setResizable(false);

        //text field
        JTextField text1 = new JTextField();
        add(text1);
        text1.setLocation(50, 20);
        text1.setSize(100, 20);
        text1.setEditable(true);
        text1.setText("");

        //text field 2
        JTextField text2= new JTextField();
        text2.setEditable(true);
        add(text2);
        text2.setLocation(50, 50);
        text2.setSize(100, 20);
        text2.setText("");

        //text area
        JTextArea area=new JTextArea();
        area.setBounds(100,200, 100,20);
        add(area);
        setSize(600,800);
        setLayout(null);
        setVisible(true);


        // button
        JButton button = new JButton("Suma");
        add(button);
        button.setBounds(10, 130, 200, 50);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String Text1, Text2;
                Text1 = text1.getText();
                Text2 = text2.getText();
                int sum = Integer.parseInt(Text1) + Integer.parseInt(Text2);
                area.setText(Integer.toString(sum));

            }
        });


    }

    public static void main(String[] args) {
        examen_isp_2020.S2 a = new examen_isp_2020.S2();
    }
}
